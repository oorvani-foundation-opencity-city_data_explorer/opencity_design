
title: Design Documentation - OpenCity
created at: Wed Mar 03 2021 05:48:03 GMT+0000 (Coordinated Universal Time)
updated at: Fri Apr 30 2021 05:50:29 GMT+0000 (Coordinated Universal Time)
---

# Design Documentation - OpenCity

**O**pen City is a city-level data platform hosted and developed by [Oorvani Foundation](https://oorvani.org/) and [Data Meet.](http://datameet.org/) The platform aims to bring visibility and transparency into local governance and has data from cities like Bengaluru, Chennai, Kolkata, Mumbai, Hyderabad, etc hosted on the platform. OpenCity contains close to 534 Datasets and 1323 Documents and has around 50,000 unique users per month.

Acting as a data repository, important data at the city level was found and posted on the platform by the members of the foundation. To make this platform more accessible to people, a re-design was planned for OpenCity. The aim of this re-design is to make the platform more accessible and user-friendly.

After collating an ample amount of insights from the research, the focus shifted to designing the platform from the directions gained from research.

Here is the documentation of the major design decisions we took as a team:

# Home Page

From the understandings gained from the research, it was decided that the home page should allow users to explore the platform and get a glimpse of the important things in the platform to explore. As the main focus of the platform is for the personas Data Browser and Data Deep diver (the former is a user who typically explores the platform at first while the latter tends to come to the platform with a specific purpose like downloading a dataset).

![Wordpress - Home Page.png](media_Design%20Documentation%20-%20OpenCity/Wordpress%20-%20Home%20Page.png)

These sections are the most prominent in the platform and were chosen based on the importance in terms of action and also how it would enable the users to explore the platform.

1.  **Emphasis on Search**
2.  **Allowing users to browse datasets by themes**
3.  **Prominent Submit Data Option **
4.  **Show Featured Datasets**
5.  **Show Featured Stories**
6.  **Show Featured Guides and explainers **
7.  **Show the Community Bulletin Board**

## Search

The search bar is positioned in such a way that it is clearly given an emphasis.

The usability analysis done on the earlier version of OpenCity showed the search bar not being in a position with emphasis and was amidst the clutter of many other sections around. A considerably large search bar is used to provide the emphasis.

![Capture.PNG](media_Design%20Documentation%20-%20OpenCity/Capture.PNG)

## Browse Datasets by theme

One of the important features that we found the need for was allowing users to browse datasets by theme. Although in the earlier version of OpenCity, datasets were divided into categories, we found that those categorizations were not relevant in some cases. The team members with the expertise in working with datasets went through all the datasets in OpenCity and came up with 8 themes. The challenge in the design was to make these themes easily accessible and visually appealing. Because browse by theme goes hand in hand with search, it was placed just below the search bar and is one of the first things the user would see upon visiting the home page. Several iterations were done on how to design this section (this can be seen in the Home Page iterations section below).

The image below shows the final design of this section.

![browse by theme.PNG](media_Design%20Documentation%20-%20OpenCity/browse%20by%20theme.PNG)

## Featured Datasets

Another feature decision that we took as a team because this is one of the features in the earlier version of opencity that after analysis of the platform that needed to be retained but designed better. The featured datasets in the earlier version of OpenCity were links displayed in the home page. But, this was placed among clutter and was not easy to draw attention to.

In the new design, the focus was to make this section appealing and more visible. Different iterations were shown to the team and we selected the following one as the final design:

![featured datasets.png](media_Design%20Documentation%20-%20OpenCity/featured%20datasets.png)

The main decision made was to limit while designing this section was to limit the number of datasets in this section to be 6. This is the number that accommodates the view in one scroll.

Another major decision was to add tags of which city the dataset represents. This is also a larger decision that is applied throughout the platform in other sections too, like blogs, explainers, etc.

## Featured Stories

As part of the goal of making the platform more attractive to users who are not data deep divers, one strategy we adopted was to incorporate more stories in the platform and make the presentation more visual. This led us to the decision of having a featured section of stories in the Home Page itself.

The design of this section had to have a visual attraction and had to give key details of the article like the city, author, date, and a short intro of the article.

After experimenting with different iterations, the one below was the final pick.

![featured blogs.png](media_Design%20Documentation%20-%20OpenCity/featured%20blogs.png)

## Featured Guides and Explainers

Similar to featured stories, it is necessary to show some of the featured Guides and Explainers on the home page. To distinguish this from the stories, the design of this section is quite different visually.

![guides and explainers.png](media_Design%20Documentation%20-%20OpenCity/guides%20and%20explainers.png)

## Community Bulletin Board

A prominent finding in the research was that users prefer to know about the citizen-driven events that are happening in their community (in this case events related to urban development). Hence we decided to have a page for featuring events called the community events page.

As our strategy was to make OpenCity appear more than just a data platform and create an interest in non-heavy consumers of data, a section was also added in the Home Page to show a few of the upcoming events on the Home Page.

Several iterations were sketched trying out how to layout the events. The design we followed was to have a shape of a bulletin board and make the containers of event details resemble posters/pages pinned on a physical notice board.

Although earlier iterations resembled this more visually, the team preferred to have more details given out on the Home page, and hence images and descriptions of the event were also added along with the event name and schedule. This led to a slight change in the design and the final design is as follows:

![bulletin board.PNG](media_Design%20Documentation%20-%20OpenCity/bulletin%20board.PNG)

## Various initial wireframe iterations of the Home Page

![Untitled-1-01.png](media_Design%20Documentation%20-%20OpenCity/Untitled-1-01.png)

# Search Results Page

The search results page is one of the most important pages in the redesign. During the analysis of the earlier version of OpenCity, the search results page was a page that needed the most improvement.

![CKAN_Search results.png](media_Design%20Documentation%20-%20OpenCity/CKAN_Search%20results.png)

From research and discussions with the client and team, the following was identified as that needs to be shown upfront in the search results page

-   Need to show file types in the dataset upfront
-   Need to show what kind of data it is (map/graph etc.)
-   Have a short description of the dataset
-   Display the source organization and theme of the dataset
-   Users can select the city about which they are looking for data or see data from all cities.

The major additions to the search results page based on the research are as follows:

## Filter for search results

Filtering the search results was something that was absent in the earlier version of opencity. In the research, we found that users are struggling to go through a long list of search results without any aid to narrow down what they need. Hence one of the primary needs on the search results page was determined to be the filter.

Various iterations of filters were made and discussed with the team.

### Here's a list of all the iterations that were explored:

## Iteration 1

![first filter.PNG](media_Design%20Documentation%20-%20OpenCity/first%20filter.PNG)

During the early wireframing, the filter was designed as a section on the left with dropdowns, the users can select the filters by ticking on the checkboxes.

As the team had initially decided to make the platform city-specific, a separate dropdown was provided for filtering by city. This is a common feature that we intended to have across the platform wherever filtering by city becomes necessary.

During the discussion with the team, the developers raised concerns about developmental challenges for this design, especially for the mobile version.

## Iteration 2

![filter 2.PNG](media_Design%20Documentation%20-%20OpenCity/filter%202.PNG)

The second iteration that was explored was to have a "show filter" option aside the search bar. The filter would expand once clicked on and the users can select the filters they need. During the discussion, although the team was very interested in this iteration, it was decided not to proceed with this because of developmental challenges.

## Iteration 3

![filter 3.PNG](media_Design%20Documentation%20-%20OpenCity/filter%203.PNG)

On further contemplation and reflecting on the developmental challenges that the dev team had mentioned, another iteration of the filter was tried out. Here, the options in the filter were taken out and were placed as separate options rather than keeping them under "show filter".

## Iteration 4

![56.PNG](media_Design%20Documentation%20-%20OpenCity/56.PNG)

In this iteration, a pop-up from the side would open as the user clicks Show filters. The dev team felt that this design would be easier to implement and the team liked this iteration from an experience point of view. Hence the team voted to take this iteration of the filter forward.

### The final version of the filter in Hi-fi

![filter_final.PNG](media_Design%20Documentation%20-%20OpenCity/filter_final.PNG)

### Designing the container of search results

The two iterations explored for the container of the search results are the following:

![search results 2.PNG](media_Design%20Documentation%20-%20OpenCity/search%20results%202.PNG)

One of the iterations was to separate the search results with dotted lines (for a cleaner look). Upon discussion with the team, the team preferred to go forward with this as the majority of the team and the client voted for this.

![search results.PNG](media_Design%20Documentation%20-%20OpenCity/search%20results.PNG)

An alternative iteration was to have the contents in a box, but because of the visual appeal of iteration 1 (with lines) and the team's preference for iteration 1, this design was not taken forward.

# Stories & Guides and Explainers

## Stories Page

One major insight that we got while interviewing users and also from the ideation workshop that we conducted was that users prefer to see stories of how data is being used by citizens and other organisations around different cities in solving civic issues. This led the team to the decision that we need to have a **Stories Page **to feature data-centered stories.

![Wordpress_Blog section.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Blog%20section.png)

## Guides and Explainers Page

Another user needs that we recognized in the research is that users prefer to know how to do certain things in their city (for eg. 'how to get a new gas connection in chennai'). This led us to the decision to have an exclusive **Guides and Explainers** page in opencity which contains articles from various cities.

![Wordpress_Guides and explainers.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Guides%20and%20explainers.png)

### **Design of these pages:**

* * *

**A similar design was made for both Stories Page and Guides and Explainers page. **

* * *

The major design decisions that we took for these pages are as follows:

**Filter articles by city: **This is a feature that is being used across the platform and the team discussed this and decided to keep this feature in the stories and guides and explainer pages too. Users can view articles from all cities or choose to filter by city.

![select city.PNG](media_Design%20Documentation%20-%20OpenCity/select%20city.PNG)

**Articles have a tag of city name: **For the users to easily identify about which city the blog post is about, a tag is shown on the blog image thumbnail.

![city tag.PNG](media_Design%20Documentation%20-%20OpenCity/city%20tag.PNG)

Different colors are used for different cities and are as follows:

Bangalore -  #6800AC

Hubli - #FA991C

Kolkata- #FC0000

Chennai - #FF1A8D

Mysore- #4AC5A5

**Users can browse articles by topic: **Since there are different topics in Citizen Matters that there are articles about. It seemed important to add the same feature in OpenCity aswell. Although most articles in OpenCity would be data-centered stories, having the option to browse by topic would help users to browse through all the contents much easily and find what interests them.

![browse.PNG](media_Design%20Documentation%20-%20OpenCity/browse.PNG)

**Search articles: ** Because of the large number of articles that would be there in OpenCity over time, we added a search bar for users to search for a particular topic/article.

![search article.PNG](media_Design%20Documentation%20-%20OpenCity/search%20article.PNG)

After discussion with the tech team, it was decided that it's best to keep the articles as three columns. It was also decided that we shall keep one of the columns as **Editor's Pick **where users can see the most relevant articles picked by the editors.

![blog columns.PNG](media_Design%20Documentation%20-%20OpenCity/blog%20columns.PNG)

* * *

**Featured Blog Articles on top: **To increase the aesthetic appeal of the blog section, featured articles was added to the top of the page. A carousel is used to cycle through the content.

The idea of having featured articles were discussed with the team, showing them other iterations without such a section. The team voted for having featured blogs and this was finalized in the design.

![featured article.PNG](media_Design%20Documentation%20-%20OpenCity/featured%20article.PNG)

## Various Iterations

![iterations.PNG](media_Design%20Documentation%20-%20OpenCity/iterations.PNG)

![latest.PNG](media_Design%20Documentation%20-%20OpenCity/latest.PNG)

During wireframing, the two iterations (shown above) were presented to the team. The major difference being the presence of the featured articles section.

**Various layouts were also explored (as shown above). Upon presentation to the team and discussions regarding ease of maintenance for Oorvani team, the team collectively preferred to have a 3 column layout of articles, with one being editor's pic section (marked separately with a different background color). It was also decided that featured articles with a carousel would be used in the Stories page and the Guides and explainers page would have a similar design but no featured articles on top.**

# Article Page

The aim while designing the article page was to keep it as minimalistic as possible so as to draw the attention of the user to the text without having many distractions.

**Major Design Decisions:**

**Center aligned text**

The text of the article page is designed to be center-aligned so that the reader can focus on the text much easier. This helps to increase readability.

**Share Article**

In the research, we found that users tend to share content that is relevant to their work with their colleagues. Some users also tend to post content from opencity on social media sites (mostly Twitter and Facebook). Hence, we decided to add a share option that allows users to share the article on Facebook, Twitter or get the link copied to the clipboard so that they can share it in any other platform they want to.

* * *

**Related Articles**

The related articles section allows users to explore similar articles and continue reading. The design of the related article section is consistent with the featured datasets section in the Homepage.

**The same design of article page is used for both blog articles on Stories page and articles on Guides and Explainers Page.**

![Wordpress_Article page.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Article%20page.png)

# Dataset Page

The dataset page is one of the most important pages in the platform and was the most challenging to design. The dataset page contains files that users can explore or download.

All the features added in the dataset page were the result of insights from the design research.

![CKAN_Dataset page_Main.png](media_Design%20Documentation%20-%20OpenCity/CKAN_Dataset%20page_Main.png)

The following is the design decisions that were taken for the dataset page.

# Sign in and Sign up Pages

![CKAN_Sign in.png](media_Design%20Documentation%20-%20OpenCity/CKAN_Sign%20in.png)

![CKAN_Sign up.png](media_Design%20Documentation%20-%20OpenCity/CKAN_Sign%20up.png)

# Submit Data Page

The submit data page allows users to submit data to OpenCity which they think should be made available to the general public via the platform. This page would be used by the users who are contributors (data uploader persona).

The fields of the data submission page were retained from the earlier version of OpenCity after a discussion with the team. The only addition was an option to submit links in case the user does not have a file in their possession / if they want to share a cloud storage link. In this case, the OpenCity team would visit the link that the user has pointed to, download the data and upload it on OpenCity for other citizens to access.

### Iterations and Design Decisions:

One major decision that had to be taken during the design of this page is whether users would be able to upload files or whether they would just be able to submit the file to OpenCity with their details and the OpenCity team would review it and they would upload the file to OpenCity.

After a series of discussions with the team about both possibilities, it was decided the user would just be able to submit the files/link to files to the OpenCity team and the team would make it available publically on OpenCity after review. The major reason for taking this decision was the ease of management for the team given the current scope. The data would hence not be posted from the profile of the person submitting but rather from the admin from the OpenCity team.

The initial iterations were as follows:

The initial exploration was to use a single page form which is similar to how the form was in the earlier version of OpenCity. Here, all the fields would appear in single page.

![single form.png](media_Design%20Documentation%20-%20OpenCity/single%20form.png)

On further ideation, the design team felt that it would be easier for the user if the data submission process was divided into different sections.

![divided into 3.png](media_Design%20Documentation%20-%20OpenCity/divided%20into%203.png)

The sections were divided as:

-   Details of the files being submitted
-   Upload the file user wants to submit/ share links

While the earlier iterations had a section which asks users for contact details (name and email), this section was removed later with the realization that contact details would be already available once the user logs in (user can be identifed because they log in ; incase they need to be contacted regarding the data) and there is no need to ask this separately each time.

![Wordpress_Submit data 1.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Submit%20data%201.png)

![Wordpress_Submit data 2.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Submit%20data%202.png)

Another design decision was taken to redirect the user to a page which shows links to datasets to explore in OpenCity. This iteration was an extension of the 'Thank you' message that would appear once the user submits the data.

The page was designed similar to explore more datasets section would look in the dataset page.

![Wordpress_Submit data 4.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Submit%20data%204.png)

# Community Bulletin Board Page

Community Bulletin Board Page is the page where users can explore and know about events in the community. This page is the extension of the community events displayed in the home page.

The thought of creating such a page came into the discussion of the team when the research insight came into light showing that users are interested to know community events in their city which they can be part of. The events featured on OpenCity would mostly be limited to events related to Open Data in the context of a city.

### Design Decisions:

The major decisions we took for this page are to separate upcoming events and have a separate section that allows users to browse previous events.

![upcoming events.PNG](media_Design%20Documentation%20-%20OpenCity/upcoming%20events.PNG)

![past events.PNG](media_Design%20Documentation%20-%20OpenCity/past%20events.PNG)

One of the design decisions that needed to be taken was whether users can post about events directly or whether users can upload details of an event and the OpenCity admin team would review it and post it on their behalf. When we discussed this with the team, the team preferred the latter option because it prevents any irrelevant content from being posted on the platform. After this discussion, we designed a form that allows users to fill up the details about the event and upload any images & links related to the event.

![tell us.png](media_Design%20Documentation%20-%20OpenCity/tell%20us.png)

The form that would appear would ask for the following details:

-   Name of event
-   Date and time of the event
-   A short description of the event
-   Images and links about the event
-   Name and email of the person submitting these details

Uppon clicking an event the user is directed to the details page of the event. This page contains the schedule, description of event and link to the event website (if available). One request from the OpenCity team was to add share options since many users would like to share the event details with others to spread the word. The share option was also added to the page and the final design is as given below:

![Wordpress_Community events_Individual Event.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_Community%20events_Individual%20Event.png)

# About Page

About page helps the user to know more about the OpenCity project and Oorvani Foundation.

What needed to be shown in the page are as follows:

-   Description of the project
-   About the foundation
-   Subscribe to newsletter
-   Donate
-   Send a message
-   Contact address

During initial iterations, subscribe to newsletter was only shown in the Home page. After a discussion with the team, we voted that we could add subscribe option aswell as an option to send a message to the OpenCity admin team in the about page.

Visually, the design attempts to be attractive and hence the use of a cover image (similar to blog section although a stand-alone image is used in this case.)

Although initially the link to the about page was kept as an option in the header, the team had a suggestion to remove the about page from the header and only keep it in the footer. This was to add Themes and Organisations options to the header as they were more likely to be used by users than the about page. The removal was also to prevent too many options being present in the header.

![About.png](media_Design%20Documentation%20-%20OpenCity/About.png)

![Wordpress_About section.png](media_Design%20Documentation%20-%20OpenCity/Wordpress_About%20section.png)

# Organizations Page

Organizations Page is a simple page that was designed after a request from the team that is a need to showcase all organizations whose data is on OpenCity.

Upon clicking an organisation's logo, the user would be shown the search results page which would show datasets from that particular organisation.

The design was kept quite simple with just the organsiation logos and a search bar in the page.

![Contributor Organisations page.png](media_Design%20Documentation%20-%20OpenCity/Contributor%20Organisations%20page.png)

# Themes Page

The themes page is another additional page added at the request of the team. The request was that there should be a separate page that would allow users to see the number of datasets in each theme and explore the themes.

As per this request from the team, a themes page was designed. The themes page was designed quite simple and followed the same style as in the sign-in page with a container holding the contents.

![Themes Page.png](media_Design%20Documentation%20-%20OpenCity/Themes%20Page.png)

# Visual Graphics

Typography

Colors

Icons

Shadow on hover

Card UI/UX

          